import React, { Component, CSSProperties } from 'react';
import { connect } from 'react-redux';
import { getIsPositionAvailable } from '../helpers/availabilityHelpers';
import { mapPieceToField, movePiece } from '../helpers/moveHelpers';
import { store } from '../redux/store';
import { Piece } from '../typescript/Interfaces';
import Field from './Field';

interface StateInterface {
  selected: Piece | null;
}

export enum BoardMode {}

interface BoardProps {
  mode?: BoardMode;
}

class Board extends Component<BoardProps> {
  constructor(props: BoardProps) {
    super(props);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  state: StateInterface = {
    selected: null,
  };

  // generates field items
  getBoardFields = () => {
    const fields = [];
    for (let row = 9; row >= 1; row--) {
      for (let column = 1; column <= 9; column++) {
        const fieldObject = {
          horizontalPosition: column,
          verticalPosition: row,
        };
        fields.push(fieldObject);
      }
    }
    return fields;
  };

  render() {
    const { gameReducer } = store.getState();
    const { player } = gameReducer;
    const boardFields = this.getBoardFields();
    return (
      <div style={{ ...(styles.row as CSSProperties) }}>
        {boardFields.map((field, fieldIndex) => {
          const { horizontalPosition, verticalPosition } = field;
          const piece = mapPieceToField(horizontalPosition, verticalPosition);
          const { selected } = this.state;
          let disabled = false;
          if (selected) {
            // if there is a preselected piece, calculates availability for all fields
            disabled = !getIsPositionAvailable(
              horizontalPosition,
              verticalPosition,
              selected
            );
          }
          return (
            <Field
              key={`field-${fieldIndex}`}
              piece={piece}
              onClick={() => {
                if (selected) {
                  // if there is a preselected piece, executes move
                  // movePiece will return if the action is not possible
                  movePiece(
                    selected,
                    horizontalPosition,
                    verticalPosition,
                    piece || undefined
                  );
                  this.setState({ selected: null });
                } else {
                  if (piece?.player != player) return;
                  this.setState({ selected: piece });
                }
              }}
              disabled={disabled}
            />
          );
        })}
      </div>
    );
  }
}

const styles = {
  row: {
    width: '468px',
    height: '468px',
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
};

const mapStateToProps = (state: any) => state;
const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Board);

//export default Board
