import React, { Component, CSSProperties } from 'react';
import { connect } from 'react-redux';
import { store } from '../redux/store';
import { PieceTypes, Player } from '../typescript/Enums';
import { Piece } from '../typescript/Interfaces';

const kingSource = require('../assets/png/king.png');
const rookSource = require('../assets/png/rook.png');
const bishopSource = require('../assets/png/bishop.png');
const goldSource = require('../assets/png/gold.png');
const silverSource = require('../assets/png/silver.png');
const knightSource = require('../assets/png/knight.png');
const lanceSource = require('../assets/png/lance.png');
const pawnSource = require('../assets/png/pawn.png');

interface StateInterface {}

// export enum FieldMode {}

interface FieldProps {
  piece: Piece | null;
  player?: Player;
  selected?: boolean;
  disabled?: boolean;
  onClick?: any;
  key: string;
}

class Field extends Component<FieldProps> {
  constructor(props: FieldProps) {
    super(props);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  state: StateInterface = {};

  getImageSource = (pieceType: PieceTypes) => {
    switch (pieceType) {
      case PieceTypes.King:
        return kingSource;
      case PieceTypes.Rook:
        return rookSource;
      case PieceTypes.Bishop:
        return bishopSource;
      case PieceTypes.Gold:
        return goldSource;
      case PieceTypes.Silver:
        return silverSource;
      case PieceTypes.Knight:
        return knightSource;
      case PieceTypes.Lance:
        return lanceSource;
      case PieceTypes.Pawn:
        return pawnSource;
      default:
        return null;
    }
  };

  render() {
    const { piece, disabled, onClick } = this.props;
    const { gameReducer } = store.getState();
    const { player } = gameReducer;
    let isPlayerB = false;
    let imageSource = null;
    let backgroundColor = disabled ? '#6E0A1E' : '#d3d3d3';
    if (piece) {
      isPlayerB = piece.player == Player.B;
      imageSource = this.getImageSource(piece?.pieceType);
      if (piece?.player == player && !disabled) {
        backgroundColor = '#ffffff';
      }
    }
    return (
      <div
        style={{
          backgroundColor,
          transform: isPlayerB ? `rotate(180deg)` : undefined,
          ...styles.container,
        }}
        onClick={onClick}
      >
        {imageSource != null && (
          <img
            src={imageSource}
            style={{ ...(styles.image as CSSProperties) }}
          />
        )}
      </div>
    );
  }
}

const styles = {
  container: {
    border: '1px solid black',
    width: '50px',
    height: '50px',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: '40px',
    height: '40px',
    objectFit: 'contain',
  },
};

const mapStateToProps = (state: any) => state;
const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Field);

//export default Field
