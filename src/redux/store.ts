import { combineReducers, createStore } from 'redux';
import GameReducer from './game/GameReducer';

const reducers = combineReducers({
  gameReducer: GameReducer,
});

export const store = createStore(reducers);

/* 

app.tsx
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar />
          <Navigation />
        </PersistGate>
      </Provider>

*/
