import { getInitialBoard } from '../../helpers/boardHelpers';
import { Player } from '../../typescript/Enums';
import { Piece } from '../../typescript/Interfaces';
import * as actionTypes from './GameActionTypes';

const initialBoard = getInitialBoard();

interface INITIAL_STATE_INTERFACE {
  round: number;
  player: Player;
  board: Piece[];
  history: Piece[][];
}

const INITIAL_STATE: INITIAL_STATE_INTERFACE = {
  round: 1,
  player: Player.A,
  board: initialBoard,
  history: [initialBoard],
};

const GameReducer = (state = INITIAL_STATE, action: any) => {
  const newState = { ...state };
  switch (action.type) {
    case actionTypes.SET_BOARD_STATE_BY_MOVING: {
      const { payload } = action;
      const { newBoard } = payload;
      newState.board = newBoard;
      newState.history.push(newBoard);
      newState.round += 1;
      if (newState.player == Player.A) {
        newState.player = Player.B;
      } else {
        newState.player = Player.A;
      }
      return newState;
    }
    default:
      return state;
  }
};

export default GameReducer;
