import { Piece } from '../../typescript/Interfaces';
import * as actionTypes from './GameActionTypes';

export const setBoardStateByMoving = (newBoard: Piece[]) => ({
  type: actionTypes.SET_BOARD_STATE_BY_MOVING,
  payload: {
    newBoard,
  },
});
