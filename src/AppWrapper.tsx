import React, { Component } from 'react';
import { Provider } from 'react-redux';
import App from './App';
import './App.css';
import { store } from './redux/store';

interface StateInterface {}

export enum AppWrapperMode {}

interface AppWrapperProps {
  mode?: AppWrapperMode;
}

class AppWrapper extends Component<AppWrapperProps> {
  constructor(props: AppWrapperProps) {
    super(props);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  state: StateInterface = {};

  render() {
    return (
      <>
        <Provider store={store}>
          <div style={{ ...styles.container }}>
            <App />
          </div>
        </Provider>
      </>
    );
  }
}

const styles = {
  container: {
    display: 'flex',
    height: '100vh',
  },
};

export default AppWrapper;

//export default App
