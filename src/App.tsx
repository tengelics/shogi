import React from 'react';
import './App.css';
import Board from './components/Board';

function App() {
  return (
    <div
      style={{
        display: 'flex',
        height: '100%',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Board />
    </div>
  );
}

export default App;
