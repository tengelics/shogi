import { setBoardStateByMoving } from '../redux/game/GameActions';
import { store } from '../redux/store';
import { Piece } from '../typescript/Interfaces';
import { getIsPositionAvailable } from './availabilityHelpers';

// master piece mover function => this one gets executed upon piece movement
export const movePiece = (
  piece: Piece,
  horizontalGoal: number,
  verticalGoal: number,
  occupyingPiece?: Piece
) => {
  const isPositionAvailable = getIsPositionAvailable(
    horizontalGoal,
    verticalGoal,
    piece
  );
  if (!isPositionAvailable) return;
  const { gameReducer } = store.getState();
  const { board } = gameReducer;
  const newBoard = [...board];
  if (occupyingPiece) {
    // if there is an already occupying piece, this drops it out
    const occupyingPieceCopy = { ...occupyingPiece };
    occupyingPieceCopy.dropout = true;
    const occupyingIndex = newBoard.findIndex(
      (piece: Piece) => piece.id == occupyingPiece.id
    );
    newBoard[occupyingIndex] = occupyingPieceCopy;
  }
  // sets the position of the movable object to its new position
  const pieceCopy = { ...piece };
  pieceCopy.horizontalPosition = horizontalGoal;
  pieceCopy.verticalPosition = verticalGoal;
  const pieceIndex = newBoard.findIndex(
    (piece: Piece) => piece.id == pieceCopy.id
  );
  newBoard[pieceIndex] = pieceCopy;
  // sets in redux
  store.dispatch(setBoardStateByMoving(newBoard));
};

// maps the pieces to the cells
export const mapPieceToField = (
  horizontalPosition: number,
  verticalPosition: number
): Piece | null => {
  const { gameReducer } = store.getState();
  const { board } = gameReducer;
  const item = board.find((piece: Piece) => {
    return (
      piece.horizontalPosition == horizontalPosition &&
      piece.verticalPosition == verticalPosition &&
      !piece.dropout
    );
  });
  if (item) {
    return item;
  } else {
    return null;
  }
};
