import { store } from '../redux/store';
import { PieceTypes, Player } from '../typescript/Enums';
import { Piece } from '../typescript/Interfaces';
import { mapPieceToField } from './moveHelpers';

interface Args {
  horizontalGoal: number;
  verticalGoal: number;
  piece: Piece;
}

// technical helper, calculates distances
const getDistances = (args: Args) => {
  const { horizontalGoal, verticalGoal, piece } = args;
  const { horizontalPosition, verticalPosition } = piece;
  const horizontalDistance = horizontalGoal - horizontalPosition;
  const verticalDistance = verticalGoal - verticalPosition;
  return { horizontalDistance, verticalDistance };
};

// technical helper, decides if goal cell is same as origin
const getIsSameField = (args: Args): boolean => {
  const { horizontalDistance, verticalDistance } = getDistances(args);
  if (horizontalDistance == 0 && verticalDistance == 0) {
    return true;
  } else {
    return false;
  }
};

// technical helper, decides whether cell belongs to the active player
const getIsOwnPiece = (args: Args): boolean => {
  const { horizontalGoal, verticalGoal } = args;
  const field = mapPieceToField(horizontalGoal, verticalGoal);
  if (field) {
    const { gameReducer } = store.getState();
    const { player } = gameReducer;
    return field?.player == player;
  } else {
    return false;
  }
};

// king specific possible steps defined here
const getIsPositionAvailableForKing = (args: Args): boolean => {
  const { horizontalDistance, verticalDistance } = getDistances(args);
  const getIsDistanceOk = (distance: number) => distance >= -1 && distance <= 1;
  const horizontalDistanceOk = getIsDistanceOk(horizontalDistance);
  const verticalDistanceOk = getIsDistanceOk(verticalDistance);
  if (horizontalDistanceOk && verticalDistanceOk) return true;
  return false;
};

// rook specific possible steps defined here
const getIsPositionAvailableForRook = (args: Args): boolean => {
  const { horizontalDistance, verticalDistance } = getDistances(args);
  if (horizontalDistance && verticalDistance == 0) return true;
  if (verticalDistance && horizontalDistance == 0) return true;
  return false;
};

// bishop specific possible steps defined here
const getIsPositionAvailableForBishop = (args: Args): boolean => {
  const { horizontalDistance, verticalDistance } = getDistances(args);
  if (Math.abs(horizontalDistance) == Math.abs(verticalDistance)) return true;
  return false;
};

// gold specific possible steps defined here
const getIsPositionAvailableForGold = (args: Args): boolean => {
  const { piece } = args;
  const { player } = piece;
  const { horizontalDistance, verticalDistance } = getDistances(args);
  const getIsDistanceOk = (distance: number) => distance >= -1 && distance <= 1;
  const horizontalDistanceOk = getIsDistanceOk(horizontalDistance);
  const verticalDistanceOk = getIsDistanceOk(verticalDistance);
  switch (player) {
    case Player.A:
      {
        const behindAside =
          verticalDistance == -1 && Math.abs(horizontalDistance) == 1;
        if (behindAside) return false;
      }
      break;
    case Player.B:
      {
        const behindAside =
          verticalDistance == 1 && Math.abs(horizontalDistance) == 1;
        if (behindAside) return false;
      }
      break;
  }
  if (horizontalDistanceOk && verticalDistanceOk) return true;
  return false;
};

// silver specific possible steps defined here
const getIsPositionAvailableForSilver = (args: Args): boolean => {
  const { piece } = args;
  const { player } = piece;
  const { horizontalDistance, verticalDistance } = getDistances(args);
  const getIsDistanceOk = (distance: number) => distance >= -1 && distance <= 1;
  const horizontalDistanceOk = getIsDistanceOk(horizontalDistance);
  const verticalDistanceOk = getIsDistanceOk(verticalDistance);
  switch (player) {
    case Player.A:
      {
        const behind = verticalDistance == -1 && horizontalDistance == 0;
        if (behind) return false;
      }
      break;
    case Player.B:
      {
        const behind = verticalDistance == 1 && horizontalDistance == 0;
        if (behind) return false;
      }
      break;
  }
  const aside = verticalDistance == 0 && Math.abs(horizontalDistance) == 1;
  if (aside) return false;
  if (horizontalDistanceOk && verticalDistanceOk) return true;
  return false;
};

// knight specific possible steps defined here
const getIsPositionAvailableForKnight = (args: Args): boolean => {
  const { piece } = args;
  const { player } = piece;
  const { horizontalDistance, verticalDistance } = getDistances(args);
  if (horizontalDistance != 1 && horizontalDistance != -1) return false;
  switch (player) {
    case Player.A:
      if (verticalDistance == 2) return true;
      break;
    case Player.B:
      if (verticalDistance == -2) return true;
      break;
  }
  return false;
};

// lance specific possible steps defined here
const getIsPositionAvailableForLance = (args: Args): boolean => {
  const { piece } = args;
  const { player } = piece;
  const { horizontalDistance, verticalDistance } = getDistances(args);
  if (horizontalDistance != 0) return false;
  switch (player) {
    case Player.A:
      if (verticalDistance >= 1) return true;
      break;
    case Player.B:
      if (verticalDistance <= -1) return true;
      break;
  }
  return false;
};

// pawn specific possible steps defined here
const getIsPositionAvailableForPawn = (args: Args): boolean => {
  const { piece } = args;
  const { player } = piece;
  const { horizontalDistance, verticalDistance } = getDistances(args);
  if (horizontalDistance != 0) return false;
  switch (player) {
    case Player.A:
      if (verticalDistance == 1) return true;
      break;
    case Player.B:
      if (verticalDistance == -1) return true;
      break;
  }
  return false;
};

// master cell availability decider function, every function uses this one
export const getIsPositionAvailable = (
  horizontalGoal: number,
  verticalGoal: number,
  piece: Piece
): boolean => {
  const args = { horizontalGoal, verticalGoal, piece };
  const isSameField = getIsSameField(args);
  const isOwnPiece = getIsOwnPiece(args);
  if (isSameField || isOwnPiece) return false;
  const { pieceType } = piece;
  switch (pieceType) {
    case PieceTypes.King:
      return getIsPositionAvailableForKing(args);
    case PieceTypes.Rook:
      return getIsPositionAvailableForRook(args);
    case PieceTypes.Bishop:
      return getIsPositionAvailableForBishop(args);
    case PieceTypes.Gold:
      return getIsPositionAvailableForGold(args);
    case PieceTypes.Silver:
      return getIsPositionAvailableForSilver(args);
    case PieceTypes.Knight:
      return getIsPositionAvailableForKnight(args);
    case PieceTypes.Lance:
      return getIsPositionAvailableForLance(args);
    case PieceTypes.Pawn:
      return getIsPositionAvailableForPawn(args);
    default:
      return false;
  }
};
