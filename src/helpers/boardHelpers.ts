import { PieceTypes, Player } from '../typescript/Enums';
import { Piece } from '../typescript/Interfaces';
import { v4 as uuidv4 } from 'uuid';

// initializes pawns to the board
const getPawns = (player: Player) => {
  const pawnArray: Piece[] = [];
  for (let i = 1; i < 10; i++) {
    const pawn: Piece = {
      id: uuidv4(),
      player,
      pieceType: PieceTypes.Pawn,
      dropout: false,
      horizontalPosition: i,
      verticalPosition: player == Player.A ? 3 : 7,
    };
    pawnArray.push(pawn);
  }
  return pawnArray;
};

// initializes kings to the board
const getKings = (player: Player) => {
  const kingArray: Piece[] = [];
  const king: Piece = {
    id: uuidv4(),
    player,
    pieceType: PieceTypes.King,
    dropout: false,
    horizontalPosition: 5,
    verticalPosition: player == Player.A ? 1 : 9,
  };
  kingArray.push(king);
  return kingArray;
};

// initializes rooks to the board
const getRooks = (player: Player) => {
  const rookArray: Piece[] = [];
  const rook: Piece = {
    id: uuidv4(),
    player,
    pieceType: PieceTypes.Rook,
    dropout: false,
    horizontalPosition: player == Player.A ? 8 : 2,
    verticalPosition: player == Player.A ? 2 : 8,
  };
  rookArray.push(rook);
  return rookArray;
};

// initializes bishops to the board
const getBishops = (player: Player) => {
  const bishopArray: Piece[] = [];
  const bishop: Piece = {
    id: uuidv4(),
    player,
    pieceType: PieceTypes.Bishop,
    dropout: false,
    horizontalPosition: player == Player.A ? 2 : 8,
    verticalPosition: player == Player.A ? 2 : 8,
  };
  bishopArray.push(bishop);
  return bishopArray;
};

// initializes golds to the board
const getGolds = (player: Player) => {
  const goldArray: Piece[] = [];
  for (let horizontal = 4; horizontal <= 6; horizontal += 2) {
    const gold: Piece = {
      id: uuidv4(),
      player,
      pieceType: PieceTypes.Gold,
      dropout: false,
      horizontalPosition: horizontal,
      verticalPosition: player == Player.A ? 1 : 9,
    };
    goldArray.push(gold);
  }
  return goldArray;
};

// initializes silvers to the board
const getSilvers = (player: Player) => {
  const silverArray: Piece[] = [];
  for (let horizontal = 3; horizontal <= 7; horizontal += 4) {
    const silver: Piece = {
      id: uuidv4(),
      player,
      pieceType: PieceTypes.Silver,
      dropout: false,
      horizontalPosition: horizontal,
      verticalPosition: player == Player.A ? 1 : 9,
    };
    silverArray.push(silver);
  }
  return silverArray;
};

// initializes knights to the board
const getKnights = (player: Player) => {
  const knightArray: Piece[] = [];
  for (let horizontal = 2; horizontal <= 8; horizontal += 6) {
    const knight: Piece = {
      id: uuidv4(),
      player,
      pieceType: PieceTypes.Knight,
      dropout: false,
      horizontalPosition: horizontal,
      verticalPosition: player == Player.A ? 1 : 9,
    };
    knightArray.push(knight);
  }
  return knightArray;
};

// initializes lances to the board
const getLances = (player: Player) => {
  const lanceArray: Piece[] = [];
  for (let horizontal = 1; horizontal <= 9; horizontal += 8) {
    const lance: Piece = {
      id: uuidv4(),
      player,
      pieceType: PieceTypes.Lance,
      dropout: false,
      horizontalPosition: horizontal,
      verticalPosition: player == Player.A ? 1 : 9,
    };
    lanceArray.push(lance);
  }
  return lanceArray;
};

// master board redux initializer function
export const getInitialBoard = () => {
  const pieces: Piece[] = [];
  const getPlayerPieceTypes = (player: Player) => {
    const pawns = getPawns(player);
    const kings = getKings(player);
    const rooks = getRooks(player);
    const bishops = getBishops(player);
    const golds = getGolds(player);
    const silvers = getSilvers(player);
    const knights = getKnights(player);
    const lances = getLances(player);
    const condensed = [
      ...pawns,
      ...kings,
      ...rooks,
      ...bishops,
      ...golds,
      ...silvers,
      ...knights,
      ...lances,
    ];
    pieces.push(...condensed);
  };
  getPlayerPieceTypes(Player.A);
  getPlayerPieceTypes(Player.B);
  return pieces;
};
