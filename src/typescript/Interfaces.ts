import { PieceTypes, Player } from './Enums';

export interface Piece {
  id: string;
  player: Player;
  pieceType: PieceTypes;
  dropout: boolean;
  horizontalPosition: number;
  verticalPosition: number;
}
