export enum Player {
  A = 'A',
  B = 'B',
}

export enum PieceTypes {
  King = 'King',
  Rook = 'Rook',
  Bishop = 'Bishop',
  Gold = 'Gold',
  Silver = 'Silver',
  Knight = 'Knight',
  Lance = 'Lance',
  Pawn = 'Pawn',
  Empty = 'Empty',
}
